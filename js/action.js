(function functionName() {
  var answer_yes = document.querySelector('.answer_yes');
  var answer_no = document.querySelector('.answer_no');
  var answers = document.querySelector('.answers');
  var description = document.querySelector('.description');
  var selected_answer = document.querySelectorAll('.selected_answer');
  var selected_answer_no = document.querySelectorAll('.selected_answer_no');
  var selected_answer_yes = document.querySelectorAll('.selected_answer_yes');
  var profile = document.querySelector('.profile');
  var undo = document.querySelectorAll('.selected_answer__undo');
  var bg = document.querySelectorAll('.bg');
  var card = document.querySelector('.card');
  var field = document.querySelectorAll('.area');
  var field_user = document.querySelectorAll('.full_name')
  var thanks = document.querySelector('.thanks');

  answer_yes.addEventListener('click', function() {
    answers.style.display = "none";
    description.style.display = "block";
    card.classList.add('card_y');

    for (var i = 0; i < selected_answer_yes.length; i++) {
      selected_answer_yes[i].style.display = 'block';
    }
    for (var i = 0; i < bg.length; i++) {
      bg[i].classList.add("bg_yes");
    }
    for (var i = 0; i < selected_answer.length; i++) {
      selected_answer[i].style.display = 'block';
    }
  })

  answer_no.addEventListener('click', function() {
    answers.style.display = "none";
    description.style.display = "block";
    card.classList.add('card_n');
    for (var i = 0; i < selected_answer_no.length; i++) {
      selected_answer_no[i].style.display = 'block';
    }
    for (var i = 0; i < bg.length; i++) {
      bg[i].classList.add("bg_no");
    }
    for (var i = 0; i < selected_answer.length; i++) {
      selected_answer[i].style.display = 'block';
    }
  })

  for (var i = 0; i < bg.length; i++) {
    bg[i].addEventListener('click', function() {
      for (var i = 0; i < field.length; i++) {
        if (field[i].value.length > 0) {
          description.style.display = "none";
          profile.style.display = "block";
        }
      }
    })
  }

  for (var i = 0; i < field.length; i++) {
    field[i].addEventListener('focus', function() {
      var save = this.placeholder;
      var characters = this.getAttribute("maxlength");
      this.placeholder = ' ..english only, please (' + characters + ')';
      this.addEventListener('keyup', function (e) {
        if (this.value.match(/[а-яА-я]/)) {
          var dirty = this.value
          var clean = dirty.replace(/[а-яА-я]/, "");
          return this.value = clean;
        }
      })

      if (!this.maxLength) {
    		var max = this.attributes.maxLength.value;
    		this.keypress = function () {
    			if (this.value.length >= max) return false;
    		};
    	}

      this.addEventListener('blur', function() {
        this.placeholder = save;
      })
    })
  }

  for (var i = 0; i < field_user.length; i++) {
    field_user[i].addEventListener('keyup', function (e) {
      if (this.value.match(/[^a-zA-Z]/)) {
        var dirty = this.value
        var clean = dirty.replace(/[^a-zA-Z]/, "");
        return this.value = clean;
      }
    });
  }

  for(i = 0; i < undo.length;i++) {
    undo[i].addEventListener('click', function(){
      answers.style.display = "block";
      for (var i = 0; i < field.length; i++) {
        field[i].value = '';
      }
      for (var i = 0; i < selected_answer_no.length; i++) {
        selected_answer_no[i].style.display = 'none';
      }

      for (var i = 0; i < selected_answer_yes.length; i++) {
        selected_answer_yes[i].style.display = 'none';
      }
      for (var i = 0; i < selected_answer.length; i++) {
        selected_answer[i].style.display = 'none';
      }
      profile.style.display = 'none';
      description.style.display = 'none';

      for (var i = 0; i < bg.length; i++) {
        if (bg[i].classList.contains("bg_no")) {
          bg[i].classList.remove("bg_no");
        }
        if (bg[i].classList.contains("bg_yes")) {
          bg[i].classList.remove("bg_yes");
        }
      }

      if (card.classList.contains("card_n")) {
        card.classList.remove("card_n");
      }
      if (card.classList.contains("card_y")) {
        card.classList.remove("card_y");
      }
    })
  }

  window.addEventListener("load", function () {
    function sendData() {
      var XHR = new XMLHttpRequest();

      var FD  = new FormData(form);

      XHR.addEventListener("load", function(event) {
        profile.style.display = 'none';
        thanks.style.display = 'block';
      });

      XHR.addEventListener("error", function(event) {
        alert('Oups! Something goes wrong.');
      });

      XHR.open("POST", "data.php");

      XHR.send(FD);
    }

    var form = document.getElementById("cetera");

    form.addEventListener("submit", function (event) {
      event.preventDefault();

      sendData();
    });
  });

})();
